import json
import os
import pandas as pd
import numpy as np
from nilm_metadata import convert_yaml_to_hdf5
from nilmtk import DataSet
from nilmtk.disaggregate import fhmm_exact, CombinatorialOptimisation as CO, Hart85

#opening the meter data for use
filepath = 'grit.json'
f = open(filepath, 'r')
raw_data = json.load(f)

#to save specific info from the meter data log
volt = list()
current = list()
power = list()
energy = list()
time = list()

#to determine the amount of energy use log
data = raw_data['hits']['hits']
l = len(data)

#to extract specific info from each log
for i in range(l):
	#assume active source from the data is generator
	temp = data[i]['_source']['data'][0]
	volt.append(temp['voltage'][0]['value'][0])
	time.append(pd.Timestamp(temp['time']))
	power.append(temp['powerSinceLast'][0]['powerSinceLast'])
	energy.append(temp['energySinceLast'][0]['energySinceLast'])

#creating the multiple index for the table
col = pd.MultiIndex.from_tuples([('power', 'active'),('energy', 'active'), ('voltage', '')], names=['physical_quantity', 'type'])

#creating the panda dataframe from the extracted data
joined = list(zip(power, energy, volt))
df = pd.DataFrame(joined, index=time, columns=col)
df.sort(inplace=True)

df.to_hdf('./grit.h5', '/building1/elec/meter1', mode='a', format='table')

#for the metadata
metadata_path = './metadata'
convert_yaml_to_hdf5(metadata_path, hdf.filename)

f.close()


grit = DataSet('./grit.h5')

co = CO()
co.import_model('co_redd.txt');

pred = {}

for i, chunk in enumerate(grit.buildings[1].elec.mains().load()):
    chunk_drop_na = chunk.dropna(axis=1, how='all')
    pred[i] = co.disaggregate_chunk(chunk_drop_na.iloc[:, 1])  #disaggregation is done on the active power only

#Using intuitive names for the appliances
pred_overall = pd.concat(pred)
pred_overall.index = pred_overall.index.droplevel()

appliance_labels = [m.label() for m in pred[0].columns.values]
pred_overall.columns = appliance_labels

pred_overall.head()