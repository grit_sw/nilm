import json
import pandas as pd
import numpy as np
import warnings

from nilmtk.disaggregate import CombinatorialOptimisation as CO, fhmm_exact
import utility

warnings.filterwarnings("ignore")

msg = {u'master': {u'configuration_IDs': [u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'], u'id': u'70:B3:D5:43:09:E9', u'time': u'2018-02-06T11:41:13.986+0100'}, u'data': [{u'activeSource': [{u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'powerSinceLast': [{u'sourceName': u'Sunhive_1', u'powerSinceLast': 6826.52978515625, u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'costSinceLast': [{u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'costSinceLast': 0.2475000023841858, u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'energySinceLast': [{u'sourceName': u'Sunhive_1', u'energySinceLast': 9.899999618530273, u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'energyTodaySource': [{u'sourceName': u'', u'energyToday': 0.0, u'sourceType': u'', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}, {u'sourceName': u'Sunhive_1', u'energyToday': 48521.19921875, u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'timeTodaySource': [{u'sourceName': u'', u'sourceType': u'', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ', u'timeToday': 0.0}, {u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ', u'timeToday': 6.3853020668029785}], u'dod': [], u'batteryCapLast': [], u'current': [{u'sourceName': u'', u'sourceType': u'', u'value': [0.0, 0.0, 0.0, 0.0, 0.0], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}, {u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'value': [4.159999847412109, 17.579999923706055, 15.390000343322754], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'fuelSinceLast': [], u'fuelHourRate': [], u'voltage': [{u'sourceName': u'', u'sourceType': u'', u'value': [0.0, 0.0, 0.0, 0.0, 0.0], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}, {u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'value': [237.10800170898438, 231.04100036621094, 241.45799255371094], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'time': u'2018-02-06T11:41:13.986+0100', u'id': u'70:B3:D5:43:09:E9', u'Type': u'1', u'powerfactor': [{u'sourceName': u'', u'sourceType': u'', u'value': [0.0, 0.0, 0.0, 0.0, 0.0], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}, {u'sourceName': u'Sunhive_1', u'sourceType': u'grid', u'value': [0.42399999499320984, 0.9160000085830688, 0.7300000190734863], u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}], u'costTodaySource': [{u'sourceName': u'Sunhive_1', u'costToday': 1213.030029296875, u'sourceType': u'grid', u'configID_FK': u'ER5K3SJPK0R21RKDAOYZLO5VNSBXDYMJ'}]}]}

def extract_log(message):
	#extract info
	data = message['data'][0]
	time = np.datetime64(data['time'])
	power = data['powerSinceLast'][0]['powerSinceLast'] 

	#create a multiple index
	column = pd.MultiIndex.from_tuples([('power', 'active')], names=['physical_quantity', 'type'])

	#combine into pandas DataFrame
	log = pd.DataFrame(power, index=[ time ], columns=column)
	
	return log

def compute_prediction(model, log):
	#uses a pretrained model for predicting new data log
	model.MIN_CHUNK_LENGTH = 1 #real time validation for a single log
	pred = model.disaggregate_chunk(log);

	#getting labels for the appliances based on the model
	appliance_labels = [ m.label() for m in pred.columns.values ]
	pred.columns = appliance_labels

	return pred

def disaggregate(message, co):
	
	log = extract_log(message)

	co_pred = compute_prediction(co, log)

	#output result
	print "\nCombinatorial Optimisation....\n"
	print co_pred.to_dict('records')
	print "\nSum of disaggregated data:", co_pred.sum(axis=1).get(0)

def load_algorithm():
	#initialize algorithm, import model and disaggregate
	#Combinatorial Optimisation
	co = CO()
	co.import_model('./co_redd.pkl')
	return co

def main():
	co = load_algorithm()
	disaggregate(msg, co)

if __name__=='__main__':
	main()