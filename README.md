### NILM for Grit Systems
Nilm analysis for Grit Systems Smart meter

## Prerequisites packages installation
Install HDF5 libraries
	
	sudo apt-get install libhdf5-serial-dev

Install git client
	
	sudo apt-get install git

Install other dependencies 
	
	sudo python2 -m pip install numpy scipy six scikit-learn pandas==0.19.2 numexpr tables matplotlib networkx==1.9 future 

	sudo python2 -m pip install ipython jupyter

	sudo apt-get install postgresql postgresql-contrib
	
	sudo apt-get install postgresql-server-dev-all
	
	sudo pip install psycopg2

	sudo pip install nose coveralls coverage hmmlearn==0.1.1

Install NILM Metadata

	git clone https://github.com/nilmtk/nilm_metadata
	cd nilm_metadata
	python2 setup.py develop

Install NILMTK

	git clone https://github.com/nilmtk/nilmtk.git
	cd nilmtk
	python setup.py develop

You may run NILMTK tests in the "nilmtk" folder to make sure the installation is successful

	cd nilmtk
	nosetests



### Running the GRIT NILM Analysis

There are four main notebooks which includes the following:

1. Preprocessing and data conversion notebook(Grit SYSTEMS NILMTK 1)

2. Preprocessing and data conversion notebook(Grit SYSTEMS NILMTK 1)-Copy 1

3. Meter Statistics and Selection (GRIT SYSTEMS NILMTK 2)

4. Disaggregation and comparison (GRIT SYSTEMS NILMTK 3)

For the training process, the REDD dataset was used, check the notebook named:
	
5. REDD Processing

Also included is a sample data format from the Grit Meter
	
	grit.json, new_data.txt

and their corresponding h5 files after conversion

	grit.h5, grit_v2.h5

Using the REDD dataset for training, the model was exported and used to disaggregate the GRIT meter data.
	
	co_redd.pkl (Using Combinatorial Optimisation)
	fhmm_redd.pkl (Using Factorial Hidden Markov Model)

Please note that another h5 dataset(iAWE.h5 and REDD.h5) was also used but not included in this folder due to their large sizes. It is available on the Grit Network drive.

Also, the script "utility.py" has been included to enable saving and loading of the FHMM model

To start the analysis,
	
	cd nilm 

	python2 -m jupyter notebook

The analysis and disaggregation is done in the four notebooks listed earlier