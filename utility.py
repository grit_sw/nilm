#Utility function for saving and loading a FHMM model
from copy import deepcopy
from nilmtk import HDFDataStore
import pickle

def export_model(fhmm, filename):
    """Saving FHMM model to file

        Parameters
        ----------
        fhmm : FHMM instance that has been used for training
        filename: file the model is saved to

    """

    # Can't pickle datastore, so convert to filenames
    exported = deepcopy(fhmm)
    
    for meter in exported.individual:
        meter.store = (meter.store.store.filename)
    pickle.dump(exported, open(filename, 'wb'))
    #print("Model exported successfully to " + filename)


def import_model(fhmm, filename):
    """Loading FHMM model

        Parameters
        ----------
        fhmm: FHMM instance
        filename: file from where the model is to be loaded

        Model from file is loaded for use in disaggregation
        """
    imported_model = pickle.load(open(filename, 'r'))
    fhmm.model = imported_model.model
    fhmm.individual = imported_model.individual

    # recreate datastores from filenames
    for meter in fhmm.individual:
        meter.store = HDFDataStore(
            meter.store)
    fhmm.predictions = imported_model.predictions
    fhmm.MIN_CHUNK_LENGTH = imported_model.MIN_CHUNK_LENGTH
    fhmm.meters = imported_model.meters
    #print("Model imported successfully from " + filename)

if '__name__'=='__main':
    pass
