from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError
#from notifications import Notification
# from serverNotifications import ServerNotification

import grit_nilm
import arrow
import threading

disag_algo = grit_nilm.load_algorithm()
#c = AvroConsumer({'bootstrap.servers': '172.16.0.108,', 'group.id': 'nilm', 'schema.registry.url': 'http://172.16.0.108:8081'})

c = AvroConsumer({'bootstrap.servers': 'dashboard.grit.systems,', 'group.id': 'my_group', 'schema.registry.url': 'http://dashboard.grit.systems:8081'})

c.subscribe(['gritServer'])
running = True
time = arrow.now()
counter = 0
while running:
    try:
        msg = c.poll(100)
        if msg:
            if not msg.error():
                message = msg.value()

                #print message
                print message['master']['configuration_IDs']
                print message['data'][0]['powerSinceLast'][0]['powerSinceLast']
                grit_nilm.disaggregate(message, disag_algo)
                print '\nend of message\n'
                
            elif msg.error().code() != KafkaError._PARTITION_EOF:
                print(msg.error())
                running = False

    except SerializerError as e:
        print("Message deserialization failed for %s: %s" % (msg, e))
        running = False

c.close()
